import requests
from  bs4 import BeautifulSoup


# url="https://ip.hao86.com/"+ip+"/"
#
# r=requests.get(url)
# r.encoding="UTF-8"
# print(r.status_code)
# # print(r.text)
#
# soup=BeautifulSoup(r.text,"html.parser")
#
# address=soup.select("body > div.comm-content-box.clearfloat > div > div.comm-content-left.clearfloat > div > div.xq_toggle > div:nth-child(2) > table:nth-child(1) > tbody > tr:nth-child(2) > td:nth-child(2)")
#
# print("ip地址",address[0].string)


def get_address_by_id(ip):

    url = "https://ip.hao86.com/" + ip + "/"

    r = requests.get(url)
    r.encoding = "UTF-8"
    # print(r.status_code)
    # print(r.text)

    soup = BeautifulSoup(r.text, "html.parser")

    address = soup.select(
        "body > div.comm-content-box.clearfloat > div > div.comm-content-left.clearfloat > div > div.xq_toggle > div:nth-child(2) > table:nth-child(1) > tbody > tr:nth-child(2) > td:nth-child(2)")

    return "ip地址", address[0].string

ip="39.163.54.0"

where_is_ip=get_address_by_id(ip)
print(where_is_ip,"我的ip地址")