from  celery import Celery
import os

from django.conf import settings


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django93.settings')
# 创建celery对象
# main 是给所有任务添加前缀信息
app=Celery(main="my_tasks")

# 加载配置文件
app.config_from_object('my_celery.config')

# 自动发现项目中所有的任务
app.autodiscover_tasks(settings.INSTALLED_APPS)

