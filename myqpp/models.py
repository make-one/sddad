from django.db import models

# Create your models here.
class ClasseModel(models.Model):
    class_name=models.CharField(max_length=222,verbose_name="班级")

    class Meta:
        db_table="cate"
        verbose_name_plural="分类表"

    def __str__(self):
        return self.class_name

class  StudentModel(models.Model):
    name=models.CharField(max_length=222,verbose_name="学生名")
    img=models.CharField(max_length=222,verbose_name="学生头像")
    dui_class=models.ForeignKey(to=ClasseModel,on_delete=models.CASCADE,verbose_name="所在班级")

    class Meta:
        db_table="student"
        verbose_name_plural="分类表"

    def __str__(self):
        return self.name