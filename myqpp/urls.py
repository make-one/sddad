from django.urls import path
from myqpp import views


urlpatterns = [
    path("showcalsss/",views.ShowClass.as_view()),
    path("class_student/<int:id>",views.StudentView.as_view()),
    path("student_upd/<int:id>",views.UpdateDel.as_view()),
    path("qn_token/",views.QNY_token.as_view())
]
