from django.shortcuts import render
from rest_framework import request
from rest_framework.views import APIView
from rest_framework.response import Response
from myqpp.models import ClasseModel, StudentModel


# Create your views here.

class ShowClass(APIView):
    def get(self, request):
        ss = ClasseModel.objects.all()
        temp = []
        for i in ss:
            temp.append({
                "id": i.id,
                "class_name": i.class_name
            })
        return Response({
            "code": 200,
            "msg": "获取班级成功",
            "data": temp,
        })


class StudentView(APIView):
    def get(self, request, id):
        s = StudentModel.objects.filter(dui_class=id).all()
        temp = []
        for i in s:
            s_temp = {
                "id": i.id,
                "name": i.name,
                "img": i.img,
            }
            temp.append(s_temp)
        return Response({
            "code": 200,
            "msg": "获取对应班级学生成功",
            "data": temp
        })

    def post(self, request):
        name = request.data.get("name")
        img = request.data.get("img")
        dui_class = request.data.get("dui_class")
        try:
            StudentModel.objects.create(name=name, img=img, dui_class=dui_class)

        except Exception as e:
            print(e, "报错添加班级学生")
            return Response({
                "code": 303,
                "msg": "收藏添加报错"
            })
        return Response({
            "code": 200,
            "msg": "添加对应学生成功"
        })

import json
class UpdateDel(APIView):
    def get(self,request,id):
        a = StudentModel.objects.filter(id=id).all()
        temp = []
        for i in a:
            a_temp = {
                "id": i.id,
                "name": i.name,
                "img": i.img,
                "dui_class":str(i.dui_class),
            }
            temp.append(a_temp)
        return Response({
            "code": 200,
            "msg": "获取修改学生成功",
            "data": temp
        })
    def put(self,request,id):
        name = request.data.get("name")
        img = request.data.get("img")
        dui_class = request.data.get("dui_class")
        print(img,"《《《《《《《《《《《")

        # python_obj = json.loads(img)
        #
        # with open('./static/'+python_obj,'wb') as f:
        #     f.write(python_obj.read())


        if StudentModel.objects.filter(id=id).update(name=name,img=img, dui_class=dui_class):
            return Response({
                "code": 200,
                "msg": "修改学生成功"
            })
        else:
            return Response({
                "code": 400,
                "msg": "修改学生失败"
            })

    def delete(self, request, id):
        user = StudentModel.objects.filter(id=id).first()
        if user:
            user.delete()
            return Response({
                "code": 200,
                "msg": "删除学生成功"
            })
        else:
            return Response({
                "code": 200,
                "msg": "删除学生失败"
            })


from utils.qiniu_token import qinqiu_token

class QNY_token(APIView):
    def get(self, request):
        token = qinqiu_token()
        return Response({
            "code": 200,
            "msg": "返回七牛云token 成功",
            "data": token
        })




