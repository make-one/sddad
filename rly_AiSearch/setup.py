from setuptools import setup,find_packages

setup(
    name="lry-aisearch",
    version="0.0.1",
    author="LiuRuiYi",
    author_email="2352129115@qq.com",
    description="This project provides an easy way to recognize faces using AI",
    # 项目gitee主页
    url="https://gitee.com/make-one/sddad",
# 你要安装的包，通过 setuptools.find_packages 找到当前目录下有哪些包
    packages=find_packages()

)