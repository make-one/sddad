import requests
from bs4 import BeautifulSoup

# r = requests.get('https://api.github.com/')
# code=r.status_code
# text=r.text
# print(code)
# print(text)

# import requests
# from bs4 import BeautifulSoup
#
# hd = {"User-Agent":"Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1 Edg/107.0.0.0"}
#
# r = requests.get("https://movie.douban.com/",headers=hd)
# r.encoding="UTF-8"
#
# soup = BeautifulSoup(r.text,'html.parser')

# print(soup.name)

# name = soup.find_all(attrs={"tr":""})
# zwname = name[0].get_text()
# print('[中文名称]',name)


# alst = soup.find_all("a")
# for a in alst:
# 	print(a.string)

# alst = soup.find_all("a")
# for a in alst:
# 	print(a.string,"\t",a.get('href'))


import requests
from bs4 import BeautifulSoup


hd = {"User-Agent":"Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1 Edg/107.0.0.0"}

r = requests.get("https://movie.douban.com/cinema/nowplaying/xian/",headers=hd)
r.encoding="UTF-8"

#获取页面信息
content = r.text

#分析页面， 获取id和电影名
soup =BeautifulSoup(content,'html.parser')
# 线找到所有的电影信息对应的li标签;


nowplaying_movie_list = soup.find_all('li',class_='list-item')

# 存储所有电影信息
movies_info=[]

# 依次遍历每一个li标签， 再次提取需要的信息
for item in nowplaying_movie_list:
	nowplaying_movie_dict = {}

	nowplaying_movie_dict['title']=item['data-title']
	nowplaying_movie_dict['id']=item['id']
	nowplaying_movie_dict['actors']=item['data-actors']
	nowplaying_movie_dict['director']=item['data-director']
	# 将获取的{'title':"名称", "id":"id号"}添加到列表中;
	movies_info.append(nowplaying_movie_dict)

for items in movies_info:
	print(items)
