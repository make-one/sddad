import jwt
from django.conf import settings

def generate_jwt(payload):
    token=jwt.encode(payload=payload,key=settings.SECRET_KEY)

    return token