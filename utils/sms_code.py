import random
import redis
from ronglian_sms_sdk import SmsSDK
import json
from my_celery.test_celery import app

accId = '8a216da8827c888b0182aaa4e6ab07bb'
accToken = "b1c1961980ac4531add6971d402a170e"
appId = '8a216da8827c888b0182aaa4e7d007c2'


@app.task
def send_message(mobile):
    sdk = SmsSDK(accId, accToken, appId)
    tid = '1'
    data=random.randint(100000,999999)
    cls=redis.Redis()

    # redis的数据类型: hash list set zset string
    # redis 存放的是:k/v
    cls.set("sms_%s"%mobile,data,ex=120)
    datas=(data,"2")

    resp = sdk.sendMessage(tid, mobile, datas)
    print("发送短信验证码响应",resp,type(resp))

    resp_json = json.loads(resp)

    return resp_json