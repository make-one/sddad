from django.shortcuts import render
from  foods.models import CategoryModel,FoodModel,BussessModel
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.

# 展示所有分类接口
class ShowCateView(APIView):
    def get(self,request):
        count=request.data.get("count")
        cs=CategoryModel.objects.all().order_by("-num")

        if count:
            cs = cs.limit(count)
        cate_list = cs.all()
        temp=[]
        for i in cate_list:
            cs_temp={
                "id":i.id,
                "name":i.name,
                "num":i.num,
            }
            temp.append(cs_temp)
        return Response({
            "code":200,
            "msg":"获取所以分类成功",
            "data":temp,
        })

# 展示分类下商家接口
class Show_buss(APIView):
    def get(self,request):
        cate_id=request.data.get("cate_id")

        bs =BussessModel.objects.filter(cid=cate_id).all()
        temp=[]
        for i in bs:
            bs_temp={
                "id":i.id,
                "cid ": i.cid,
                "title":i.title,
                "img": i.img,
                "fen": i.fen,
                "start_time ": i.start_time ,
                "state": i.state,
            }
            temp.append(bs_temp)
        return Response({
            "code":200,
            "msg":"获取所属分类下商家成功",
            "data":temp,
        })

# 展示分类下商家评论接口
from userlogin.models import CommentModel
from utils.LoginTools import check_login
class Show_Busscomment(APIView):
    def get(self,request):
        buss_id = request.data.get("id")
        bs=CommentModel.objects.filter(buss_id=buss_id).all()

        temp=[]
        for i in bs:
            bs_temp={
                "id":i.id,
                "user":i.user_id.username,
                "buss":i.buss_id.title,
                "context":i.context,
            }
            temp.append(bs_temp)
        return Response({
            "code":200,
            "msg":'获取商家评论成功',
            "data":temp,
        })

    @check_login
    def post(self,request):
        user_id=request.id
        buss_id = request.data.get("id")
        context= request.data.get("context")
        parent_id =request.data.get("parent_id")
        root_id=request.data.get("root_id")

        buss_info = BussessModel.objects.filter(id=buss_id).first()
        if not buss_info:
            return Response({
                "code":302,
                "msg":"评论的商家不存在"
            })

        if parent_id:
            parent_info = CommentModel.objects.filter(id =parent_id).first()
            if not parent_info:
                return Response({
                    "code":302,
                    "msg":"回复的评论不存在",
                })

        if root_id and parent_id != root_id:
            root_info = CommentModel.objects.filter(id =root_id).first()
            if not root_info:
                return Response({
                    "code":301,
                    "msg": "回复的根评论不存在"
                })
        try:
            CommentModel.objects.create(user_id=user_id,buss_id=buss_id,context=context,parent_id=parent_id,root_id=root_id)
        except Exception as e:
            print(e,"用户评论失败报错")
            return Response({
                "code": 302,
                "msg": '用户评论商家报错'
            })
        return Response({
            "code": 200,
            "msg": '用户评论商家成功'
        })

# 展示分类下商家菜品接口
class Show_Food(APIView):
    def get(self,request):
        buss_id=request.data.get("id")

        fs=FoodModel.objects.filter(bussess_id=buss_id).all()
        temp=[]
        for i in fs:
            fs_temp={
                "id":i.id,
                "bussess_id":i.bussess_id,
                "name": i.name,
                "food_img": i.bussess_id,
                "price": i.price,
                "comment": i.comment,
                "pay_count": i.pay_count,
                "cun_count": i.cun_count,
            }
            temp.append(fs_temp)

        return  Response({
            "code":200,
            "msg":"获取对应商家菜品成功",
            "data":temp,
        })

class HotBusseView(APIView):
    def get(self,request):
        size=request.data.get("size")
        buss_list = BussessModel.objects.all().order_by("-fen").limit(size).all()

        buss_all = []
        for item in buss_list:
            buss_all.append({
                "bid": item.id,
                "title": item.title,
                "img": item.img.split("!")[0],
                "start_time": item.start_time,
                "fen": item.fen,
                "address": item.address,
                "phone": item.phone,
                "state": item.state,
            })
        return Response({
                "code": 200,
                "msg": "获取热门商家数据成功",
                "data": buss_all,
            })

class RecommBussessView(APIView):
    def get(self,request):
        size=request.data.get("size")

        buss_list =BussessModel.objects.filter(is_Recomm =1).limit(size).all()

        buss_all = []
        for item in buss_list:
            buss_all.append({
                "bid": item.id,
                "title": item.title,
                "img": item.img.split("!")[0],
                "start_time": item.start_time,
                "fen": item.fen,
                "address": item.address,
                "phone": item.phone,
                "state": item.state,
            })

        return Response({
                "code": 200,
                "msg": "获取推荐商家数据成功",
                "data": buss_all,
            })


